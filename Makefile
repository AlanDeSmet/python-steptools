VERSION:=$(shell awk -F'"' '/^__version__ *=/ {print $$2}' steptools/__init__.py )
PACKAGE_NAME:=$(shell awk -F= '/^name *=/ { sub(/^[ \t]+/,"", $$2);print $$2}' setup.cfg )

TARBALL:=dist/$(PACKAGE_NAME)-$(VERSION).tar.gz
UNNEEDED_FILES:=$(PACKAGE_NAME)-$(VERSION).egg-info __pycache__ dist build-env test-env build.log


all: 
	make clean
	make test
	make build
	make test-local-install

showvars:
	@echo "version: $(VERSION)"
	@echo "package: $(PACKAGE_NAME)"
	@echo "tarball: $(TARBALL)"


.PHONY: all clean test build tag-version
.PHONY: test-local-install test-remote-install release release-to-test
.PHONY: showvars


################################################################################

clean:
	rm -rf $(UNNEEDED_FILES)

################################################################################

test:
	python3 -m steptools.test

################################################################################

tag-version:
	git tag "v$(VERSION)"

################################################################################

build: $(TARBALL)

$(TARBALL): build-env/ready
	build-env/bin/python3 -m build >> build.log

build-env/ready:
	python3 -m venv build-env
	build-env/bin/pip3 --quiet install --upgrade pip
	build-env/bin/pip3 --quiet install --upgrade build
	build-env/bin/pip3 --quiet install --upgrade twine
	touch $@

################################################################################

test-local-install: test-local-env/pip3 $(TARBALL)
	test-local-env/bin/pip3 --quiet install dist/steptools_AlanDeSmet-*-py3-none-any.whl
	# Work in a subdirectory so we don't accidentally use the working copy
	(cd test-local-env && bin/python3 -m steptools.test)

test-local-env/pip3:
	python3 -m venv test-local-env
	test-local-env/bin/pip3 --quiet install --upgrade pip

################################################################################

test-remote-install: test-remote-env/pip3 dist/released-to-test
	test-remote-env/bin/pip3 --quiet install --index-url https://test.pypi.org/simple/ --no-deps $(PACKAGE_NAME)
	# Work in a subdirectory so we don't accidentally use the working copy
	(cd test-remote-env && bin/python3 -m steptools.test)

test-remote-env/pip3:
	python3 -m venv test-remote-env
	test-remote-env/bin/pip3 --quiet install --upgrade pip

################################################################################

release-to-test: 
	make clean
	make test
	make build
	make test-local-install
	make dist/released-to-test
	make test-remote-install

dist/released-to-test: $(TARBALL)
	build-env/bin/python3 -m twine upload --repository testpypi dist/*
	touch $@

release: 
	echo "You might try"
	echo "build-env/bin/python3 -m twine upload --repository testpypi dist/*"

