""" steptools

MIT License

Copyright (c) 2021 Alan De Smet

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
# SPDX-License-Identifier: MIT

__version__ = "0.1.3"


def range(start, stop=None, step=1, inclusive=False, zero_step=None):
    """ Return object producing sequence of items from start (inclusive) to stop (exclusive) in step increments

    steptools.range is as a drop-in replacement for range, adding with
    support for anything that behaves reasonably like a number, including
    floats, datetime.datetime, datetime.date, fractions.Fraction, and
    decimal.Decimal.

    if stop is None, start will be used as stop and start will be "zero"
    (type(start)()).

    Similar to range, generated items are EXCLUSIVE of stop.
    inclusive=True allows stop to be returned, assuming it's a multiple
    of step.

    type(step)() should construct a "zero" version of step which has no result
    if added to start. If this does not work, set zero_step explicitly.

    The following must work, returning reasonable results and not raising
    exceptions.
    
    if zero_step is None:
        type(step)() # must be logically "zero"

    step == zero_step

    if step < zero_step:
        stop < start
        stop < (start+step)
    else:
        start < stop
        (start+step) < stop
    if inclusive:
        start == stop
        (start+step) == stop
    if stop is none:
        type(start)() # must be logically "zero"
        

    >>> list(range(3))
    [0, 1, 2]
    >>> list(range(3, inclusive=True))
    [0, 1, 2, 3]
    >>> list(range(5, -3, -2))
    [5, 3, 1, -1]
    >>> list(range(1.1, 1.5, .2))
    [1.1, 1.3]

    >>> from datetime import datetime, timedelta
    >>> list(range(datetime(2000, 1, 1), datetime(1999,12,30),
    ...     timedelta(days=-1)))
    [datetime.datetime(2000, 1, 1, 0, 0), datetime.datetime(1999, 12, 31, 0, 0)]

    >>> from fractions import Fraction
    >>> list(range(Fraction(1,3), Fraction(2,3), Fraction(1,6)))
    [Fraction(1, 3), Fraction(1, 2)]
    >>> list(range(Fraction(1,3), Fraction(5,3)))
    [Fraction(1, 3), Fraction(4, 3)]

    >>> from decimal import Decimal
    >>> list(range(Decimal("1.33"), Decimal("1.66"), Decimal("0.11")))
    [Decimal('1.33'), Decimal('1.44'), Decimal('1.55')]
    """
    import operator

    if zero_step is None:
        try:
            zero_step = type(step)()
        except Exception as e:
            raise TypeError(f"Unable to contruct zero of the step type {type(step)}; you may need to explicitly set the zero_step value. (Error encountered: {str(e)}")


    try:
        is_zero = step == zero_step
    except Exception as e:
        raise ValueError(f"Unable to compare step ({type(step)}) to zero_step ({type(zero_step)}) for equality; you may need to explicitly set the zero_step value. (Error encountered: {str(e)}")
    if is_zero:
        raise ValueError("steptools.range() arg 3 (step) must not be zero")

    if stop is None:
        stop = start
        try:
            start = type(stop)()
        except Exception as e:
            raise TypeError(f"Unable to construct zero of type {type(stop)}; you may need to explicitly set the start value. (Error encountered: {str(e)})")

    try: # Ensure start and stop are compatible
        _ = start < stop
        if inclusive: # Only needed for inclusive
            _ = start == stop
    except Exception as e:
        raise TypeError(f"Unable to compare start ({type(tmp)} {tmp}) to stop ({type(stop)} {stop}). The types must be compatible.")

    try: # Ensure start and step are compatible
        tmp = start + step
    except Exception as e:
        msg = f"Unable to add start ({type(start)}) to step ({type(step)}). The two types must be compatible."
        if step == 1:
            msg += " You may need to explicitly set step."
        raise TypeError(msg)

    try: # Ensure (start+step) and stop are compatible
        _ = tmp < stop
        if inclusive: # Only needed for inclusive
            _ = tmp == stop
    except Exception as e:
        raise TypeError(f"Unable to compare start+step ({type(tmp)} {tmp}) to stop ({type(stop)} {stop}). The types must be compatible.")

    try:
        # Invert test instead of using operator.lt/gt to reduce
        # requirements: start>stop isn't required to work.
        compare = lambda left,right: left < right
        if step < zero_step:
            compare = lambda left,right: right < left
    except Exception as e:
        raise ValueError(f"Unable to compare step ({type(step)}) to zero_step ({type(zero_step)}) for using less than (step < zero_step); you may need to explicitly set the zero_step value. (Error encountered: {str(e)}")

    current = start
    while compare(current, stop):
        yield current
        current += step
    if inclusive and current == stop:
        yield current
